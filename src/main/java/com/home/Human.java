package com.home;

public class Human {

    private String name;
    private String surname;
    private String patronymic;

    public Human(String name, String surname) {
        this.name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        this.surname = surname.substring(0, 1).toUpperCase() + surname.substring(1).toLowerCase();
    }

    public Human(String name, String surname, String patronymic) {
        this.name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        this.surname = surname.substring(0, 1).toUpperCase() + surname.substring(1).toLowerCase();
        this.patronymic = patronymic.substring(0, 1).toUpperCase() + patronymic.substring(1).toLowerCase();
    }

    public String getFullName() {
        return surname + " " + name + " " + (patronymic == null ? "" : patronymic);
    }

    public String getShortName() {
        return surname + " " + name.charAt(0) + "." + (patronymic == null ? "" : patronymic.charAt(0) + ".");
    }
}
