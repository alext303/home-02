package com.home.circle;

import lombok.Getter;

public class Circle {

    @Getter
    private Point point;
    @Getter
    private int radius;

    public Circle(int x, int y, int radius) {
        point = new Point(x, y);
        this.radius = radius;
    }
}
