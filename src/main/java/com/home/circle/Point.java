package com.home.circle;

import lombok.Getter;

public class Point {

    @Getter
    private int x;
    @Getter
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getDistance(Point point) {
        return (int) Math.sqrt(Math.pow(this.x - point.x, 2) + Math.pow(this.y - point.y, 2));
    }

    public boolean getPointsInCircle(Circle circle) {
        return getDistance(circle.getPoint()) <= circle.getRadius();
    }
}
