package com.home.circle;

import java.util.Arrays;
import java.util.List;

public class PointList {

    private Point[] array;

    public PointList() {
        array = new Point[0];
    }

    public void add(Point point) {
        Point[] listTemp = new Point[array.length + 1];
        for (int i = 0; i < array.length + 1; i++)
            if (array.length == i)
                listTemp[i] = point;
            else
                listTemp[i] = array[i];

        array = listTemp;
    }

    public List<Point> getList() {
        return Arrays.asList(array);
    }

    @Override
    public String toString() {
        return Arrays.toString(array);
    }
}
