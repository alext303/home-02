package com.home;

import com.home.circle.Circle;
import com.home.circle.Point;
import com.home.circle.PointList;

public class Main {

    public static void main(String[] args) {
        Hello hello = new Hello();

        MyScanner scanner = new MyScanner();
        int x;
        int y;
        int radius;

        PointList points = new PointList();

        while (true) {
            System.out.println("Input 'x' and 'y' point (int)");
            System.out.print("x: ");
            x = scanner.getInt();
            System.out.print("y: ");
            y = scanner.getInt();

            points.add(new Point(x, y));

            System.out.println("Add one more? (1 - yes; 2 - no)");
            if (scanner.getInt() == 2)
                break;
        }

        System.out.println("Now input 'x', 'y' and 'radius' circle (int)");
        System.out.print("x: ");
        x = scanner.getInt();
        System.out.print("y: ");
        y = scanner.getInt();
        System.out.print("radius: ");
        radius = scanner.getInt();

        Circle circle = new Circle(x, y, radius);

        for (Point point : points.getList()) {
            if (point.getPointsInCircle(circle))
                System.out.println("x:" + point.getX() + " y:" + point.getY() + " IN CIRCLE!");
            else
                System.out.println("x:" + point.getX() + " y:" + point.getY() + " NOT IN CIRCLE!");
        }
    }
}
