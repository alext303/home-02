package com.home;

import java.util.Scanner;

public class MyScanner {

    private Scanner scanner = new Scanner(System.in);

    public int getInt(){
        while (true) {
            try {
                return Integer.parseInt(scanner.nextLine());
            } catch (Exception ignore) {
                System.out.println("Wrong input int!");
            }
        }
    }

    public String getString(){
        while (true) {
            try {
                return scanner.nextLine();
            } catch (Exception ignore) {
                System.out.println("Wrong input!");
            }
        }
    }

    public Double getDouble(){
        while (true) {
            try {
                return Double.parseDouble(scanner.nextLine());
            } catch (Exception ignore) {
                System.out.println("Wrong input double!");
            }
        }
    }
}
