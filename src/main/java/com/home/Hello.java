package com.home;

public class Hello {

    private Human human;
    private MyScanner myScanner = new MyScanner();

    public Hello() {
        String name;
        String surname;
        String patronymic;
        System.out.println("Enter name: ");
        name = myScanner.getString();
        System.out.println("Enter surname: ");
        surname = myScanner.getString();
        System.out.println("Enter surname (if not patronymic - press empty enter): ");
        patronymic = myScanner.getString();
        human = patronymic.isEmpty() ? new Human(name, surname) : new Human(name, surname, patronymic);
        System.out.println("Hello Mr(Ms): - " + human.getShortName());
    }
}
